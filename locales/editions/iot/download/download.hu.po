#. extracted from content/editions/iot/download.yml
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-26 13:33-0700\n"
"PO-Revision-Date: 2022-09-21 10:31+0000\n"
"Last-Translator: Hoppár Zoltán <hopparz@gmail.com>\n"
"Language-Team: Hungarian <https://translate.fedoraproject.org/projects/"
"fedora-websites-3-0/editionsiotdownloads/hu/>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Translate Toolkit 3.7.3\n"

#: path
msgid "download"
msgstr ""

#: title
msgid "IOT Download"
msgstr ""

#~ msgid "Fedora IoT Downloads"
#~ msgstr "Fedora IoT Letöltések"
